#include "state.h"

State::State()
{}

State::State(int px, int py): x(px), y(py){}

//relational values
bool State::operator==(const State& that) const
{
    return x == that.x and y == that.y;
}

bool State::operator<(const State& that) const
{
    return x < that.x or (not (that.x < x) and y < that.y);
}

bool State::operator>(const State& that) const
{
    return x > that.x or (not (that.x > x) and y > that.y);
}

State::const_iterator State::successors_begin() const
{
    return const_iterator(*this);
}

State::const_iterator State::successors_end() const
{
    return const_iterator();
}

State::iterator State::successors_begin()
{
    return iterator(*this);
}

State::iterator State::successors_end()
{
    return iterator();
}

double StateDistance::operator()(const State& from, const State& to) const
{
    return std::sqrt(std::pow(from.x - to.x, 2) + std::pow(from.y - to.y, 2));
}

bool StateIterator::operator==(const StateIterator& that) const
{
    return (end_ and that.end_) or (not end_ and not that.end_ and base_ == that.base_ and current_ == that.current_);
}

bool StateIterator::operator!=(const StateIterator& that) const
{
    return not operator ==(that);
}

StateIterator::StateIterator(): end_(true)
{}

StateIterator::reference StateIterator::operator*() const
{
    return current_;
}

StateIterator::pointer StateIterator::operator->() const
{
    return &current_;
}

StateIterator& StateIterator::operator++()
{
    if (end_)
        return *this;


    if (current_.x < base_.x and current_.y < base_.y)
        current_.y = base_.y;
    else if (current_.x < base_.x and current_.y == base_.y)
        current_.y = base_.y + 1;
    else if (current_.x < base_.x)
    {
        current_.x = base_.x;
        current_.y = base_.y - 1;
    }
    else if (current_.x == base_.x and current_.y < base_.y)
        current_.y = base_.y + 1;
    else if (current_.x == base_.x)
    {
        current_.x = base_.x + 1;
        current_.y = base_.y - 1;
    }
    else if (current_.y < base_.y)
        current_.y = base_.y;
    else if (current_.y == base_.y)
        current_.y = base_.y + 1;
    else
        end_ = true;


    return *this;
}

//preincrement
StateIterator StateIterator::operator++(int)
{
    StateIterator out(*this);
    operator ++();
    return out;
}

StateIterator::StateIterator(const State& s): base_(s), current_(s.x - 1, s.y - 1), end_(false)
{}
