#include <list>
#include <map>
#include <nav_core/base_global_planner.h>
#include <costmap_2d/costmap_2d.h>
#include "state.h"

namespace astar
{
class Astar: public nav_core::BaseGlobalPlanner
{
public:
    Astar();
    /**
         * @brief Given a goal pose in the world, compute a plan
         * @param start The start pose
         * @param goal The goal pose
         * @param plan The plan... filled by the planner
         * @return True if a valid plan was found, false otherwise
         */
    virtual bool makePlan(const geometry_msgs::PoseStamped& start,
                          const geometry_msgs::PoseStamped& goal, std::vector<geometry_msgs::PoseStamped>& plan);

    /**
         * @brief  Initialization function for the BaseGlobalPlanner
         * @param  name The name of this planner
         * @param  costmap_ros A pointer to the ROS wrapper of the costmap to use for planning
         */
    virtual void initialize(std::string name, costmap_2d::Costmap2DROS* costmap_ros);

private:
    bool initialized_;
    costmap_2d::Costmap2DROS* costmap_ros_;
    ros::Publisher plan_publisher_;

    double max_distance_;
    bool smoothed_;

    double cost(const State& from, const State& to, const costmap_2d::Costmap2D& costmap) const;        
    unsigned char cost(const State& s, const costmap_2d::Costmap2D& costmap) const;
    bool admissible(const State& s, const costmap_2d::Costmap2D& costmap) const;
    void publishPath(const std::vector<geometry_msgs::PoseStamped>& plan);

    void computeInputs(const std::vector<geometry_msgs::PoseStamped>& plan, std::vector<tf::Point>& input) const;    
    std::list<State> smooth(const std::list<State>& path, const costmap_2d::Costmap2D& costmap) const;
    bool lineOfSight(const State& from, const State& to,  const costmap_2d::Costmap2D& costmap) const;

};

template <class Vertex, class BinaryFunction, class UnaryPredicate, class Heuristic>
void computePath(const Vertex& start, const Vertex& goal, BinaryFunction cost, UnaryPredicate isAdmissible, Heuristic h, std::list<Vertex>& path)
{
    typedef std::pair<double, Vertex> GValue;
    typedef std::map<Vertex, GValue> GValueMap;

    GValueMap g; //keeps the distance from each state to the start state
    typedef std::pair<double, Vertex> PQElement;
    std::priority_queue<PQElement, std::vector<PQElement>, std::greater< PQElement > > open;

    g.insert(std::make_pair(start, GValue(0.0, start)));
    open.push(PQElement(h(start, goal), start));

    while (not open.empty())
    {
        Vertex s = open.top().second;
        open.pop();

        if (s == goal) //goal reached
            break;

        typename GValueMap::const_iterator gs_iterator = g.find(s);

        if (gs_iterator == g.end())
            throw std::runtime_error("G value of any open value must be defined");

        for (typename State::const_iterator sp = s.successors_begin(); sp != s.successors_end(); ++sp)
        {

            if (isAdmissible(*sp))
            {

                double candidate_g = gs_iterator->second.first + cost(s, *sp);
                std::pair<typename GValueMap::iterator, bool> gsp_value = g.insert(std::make_pair(*sp, GValue(candidate_g, s)));

                if (not gsp_value.second and gsp_value.first->second.first > candidate_g)
                { //opened before buth with a worse g_value

                    gsp_value.first->second = std::make_pair(candidate_g, s);
                    open.push(PQElement(candidate_g + h(*sp, goal), *sp));

                }
                else if (gsp_value.second) //not opened before
                    open.push(PQElement(candidate_g + h(*sp, goal), *sp));
            }
        }
    }


    Vertex current = goal;
    while (not (current == start))
    {
        path.push_front(current);
        typename GValueMap::const_iterator gs_iterator = g.find(current);
        current = gs_iterator->second.second;
    }

    path.push_front(start);
}
}
