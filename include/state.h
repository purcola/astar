#ifndef STATE_H
#define STATE_H

#include <iterator>
#include <cmath>

class StateIterator;

class State
{
public:
    //data
    int x, y;    

    //constructors
    State();
    State(int px, int py);

    //relational values
    bool operator==(const State& that) const;
    bool operator<(const State& that) const;
    bool operator>(const State& that) const;

    //iterators
    typedef StateIterator iterator;
    typedef StateIterator const_iterator;

    const_iterator successors_begin() const;
    const_iterator successors_end() const;

    iterator successors_begin();
    iterator successors_end();
};

struct StateDistance
{
    double operator()(const State& from, const State& to) const;
};

class StateIterator
{
    public:
        //types
        typedef State value_type;
        typedef unsigned int distance_type;
        typedef std::input_iterator_tag iterator_category;
        typedef const State* pointer;
        typedef const State& reference;


    //equally comparable
    bool operator==(const StateIterator& that) const;
    bool operator!=(const StateIterator& that) const;

    //default constructible
    StateIterator();

    //derefenrence
    reference operator*() const;

    //member access
    pointer operator->() const;

    //post increment
    StateIterator& operator++();

    //preincrement
    StateIterator operator++(int);

    StateIterator(const State& s);    
private:
    const State base_;
    State current_;
    bool end_;
};

#endif // STATE_H
